<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>一時預かり登録画面</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <link href="style.css" rel="stylesheet" type="text/css">
</head>

<body>
    <h1 class="text-center text-dark mt-3 mb-3">一時預かり 登録</h1>

    <div class="spotresistercontainer">
        <div class="panel panel-default">

            <div class="panel-body">
                <form class="form-horizontal" action="Spot_resister" method="post">
                <input name="today" type="hidden" value="${day}">
                    <div class="form-group has-error">
                        <label class="col-sm-5 control-label">名前：</label>
                        <div class="col-sm-12">
                            <input class="form-control" type="text" name="name">
                        </div>
                    </div>
                    <div class="form-group has-error">
                        <label class="col-sm-5 control-label">年齢：</label>
                        <div class="col-sm-4">
                            <select class="form-control" name="age">
                                <option>０</option>
                                <option>１</option>
                                <option>２</option>
                                <option>３</option>
                                <option>４</option>
                                <option>５</option>
                            </select>
                        </div>
                    </div>


                    <div class="form-group mt-5">
                        <div class="col-sm-offset-2 col-sm-12">
                            <button type="submit" class="btn btn-info ">登録</button>
                            <a href="Index" class="btn btn-info float-right">戻る</a>

                        </div>


                    </div>
                </form>
            </div>
        </div>
    </div>



</body>

</html>
