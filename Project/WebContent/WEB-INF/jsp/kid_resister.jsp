<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>月極め園児登録画面</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <link href="style.css" rel="stylesheet" type="text/css">
</head>

<body>
    <h1 class="text-center text-dark mt-3 mb-3">月極め園児 新規登録</h1>

    <div class="resistercontainer">
        <div class="panel panel-default">

            <div class="panel-body">
                <form class="form-horizontal" action="Kid_resister" method="post">
                    <div class="form-group has-error">
                        <label class="col-sm-2 control-label">名前：</label>
                        <div class="col-sm-12">
                            <input class="form-control" type="text" name="name">
                        </div>
                    </div>
                    <div class="form-group has-error">
                        <label class="col-sm-2 control-label">年齢：</label>
                        <div class="col-sm-2">
                            <select class="form-control" name="age">
                                <option>０</option>
                                <option>１</option>
                                <option>２</option>
                                <option>３</option>
                                <option>４</option>
                                <option>５</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group has-error">
                        <label class="col-sm-2 control-label">曜日：</label>
                        <div class="col-sm-12 checkbox">
                            <label><input type="checkbox" name="day1" value="1">月曜日</label>
                            <input name="day1" type="hidden" value="2">
                            <label><input type="checkbox" name="day2" value="1">火曜日</label>
                            <input name="day2" type="hidden" value="2">
                            <label><input type="checkbox" name="day3" value="1">水曜日</label>
                            <input name="day3" type="hidden" value="2">
                            <label><input type="checkbox" name="day4" value="1">木曜日</label>
                            <input name="day4" type="hidden" value="2">
                            <label><input type="checkbox" name="day5" value="1">金曜日</label>
                            <input name="day5" type="hidden" value="2">
                            <label><input type="checkbox" name="day6" value="1">土曜日</label>
                            <input name="day6" type="hidden" value="2">
                            <label><input type="checkbox" name="day7" value="1">日曜日</label>
                            <input name="day7" type="hidden" value="2">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">備考：</label>
                        <div class="col-sm-12">
                            <textarea class="form-control"></textarea>

                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 mt-5">
                            <button type="submit" class="btn btn-info float-left ml-3">登録</button>
                            <a href="Kid_list" class="btn btn-info float-right mr-3">戻る</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

</body>

</html>
