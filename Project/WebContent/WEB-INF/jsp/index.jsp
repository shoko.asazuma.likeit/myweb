<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
        <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>週間予定</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <link href="style.css" rel="stylesheet" type="text/css">
</head>


<body>
    <div class="container">
        <h1 class="text-left text-dark mt-3">今週の予定</h1>

        <div class="text-right">
            <a href="Kid_list" class="btn btn-info  btn-lg mb-3 py-1">月極め園児</a>
            <a href="Staff_list" class="btn btn-info btn-lg mb-3 py-1">先生</a>

        </div>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th scope="col" class="day">月</th>
                    <th scope="col" class="day">火</th>
                    <th scope="col" class="day">水</th>
                    <th scope="col" class="day">木</th>
                    <th scope="col" class="day">金</th>
                    <th scope="col" class="day">土</th>
                    <th scope="col" class="day">日</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>先生：　
                    <c:choose>
                    <c:when test="${cntAll[1]/3<=cntCer[1]&&cntAll[1]>=2}">◯</c:when>
                    <c:otherwise>×</c:otherwise>
                    </c:choose>
                    <br>
                        ＜受け入れ＞<br>
                        ５才：　
                        <c:choose><c:when test="${pointRemainList[1]>=2}">◯</c:when>
                    	<c:otherwise>×</c:otherwise></c:choose><br>
                        ４才：　
                        <c:choose><c:when test="${pointRemainList[1]>=2}">◯</c:when>
                    	<c:otherwise>×</c:otherwise></c:choose><br>
                        ３才：　
                        <c:choose><c:when test="${pointRemainList[1]>=3}">◯</c:when>
                    	<c:otherwise>×</c:otherwise></c:choose><br>
                        ２才：　
                        <c:choose><c:when test="${pointRemainList[1]>=10}">◯</c:when>
                    	<c:otherwise>×</c:otherwise></c:choose><br>
                        １才：　
                        <c:choose><c:when test="${pointRemainList[1]>=10}">◯</c:when>
                    	<c:otherwise>×</c:otherwise></c:choose><br>
                        ０才：　
                        <c:choose><c:when test="${pointRemainList[1]>=20}">◯</c:when>
                    	<c:otherwise>×</c:otherwise></c:choose><br>
                        <br>
                    </td>


                    <td>先生：　
                   <c:choose>
                    <c:when test="${cntAll[2]/3<=cntCer[2]&&cntAll[2]>=2}">◯</c:when>
                    <c:otherwise>×</c:otherwise>
                    </c:choose>
                    <br>
                        ＜受け入れ＞<br>
                        ５才：　
                        <c:choose><c:when test="${pointRemainList[2]>=2}">◯</c:when>
                    	<c:otherwise>×</c:otherwise></c:choose><br>
                        ４才：　
                        <c:choose><c:when test="${pointRemainList[2]>=2}">◯</c:when>
                    	<c:otherwise>×</c:otherwise></c:choose><br>
                        ３才：　
                        <c:choose><c:when test="${pointRemainList[2]>=3}">◯</c:when>
                    	<c:otherwise>×</c:otherwise></c:choose><br>
                        ２才：　
                        <c:choose><c:when test="${pointRemainList[2]>=10}">◯</c:when>
                    	<c:otherwise>×</c:otherwise></c:choose><br>
                        １才：　
                        <c:choose><c:when test="${pointRemainList[2]>=10}">◯</c:when>
                    	<c:otherwise>×</c:otherwise></c:choose><br>
                        ０才：　
                        <c:choose><c:when test="${pointRemainList[2]>=20}">◯</c:when>
                    	<c:otherwise>×</c:otherwise></c:choose><br>
                        <br>
                    </td>

                    <td>先生：　
                     <c:choose>
                    <c:when test="${cntAll[3]/3<=cntCer[3]&&cntAll[3]>=2}">◯</c:when>
                    <c:otherwise>×</c:otherwise>
                    </c:choose>
                    <br>
                        ＜受け入れ＞<br>
                        ５才：　
                        <c:choose><c:when test="${pointRemainList[3]>=2}">◯</c:when>
                    	<c:otherwise>×</c:otherwise></c:choose><br>
                        ４才：　
                        <c:choose><c:when test="${pointRemainList[3]>=2}">◯</c:when>
                    	<c:otherwise>×</c:otherwise></c:choose><br>
                        ３才：　
                        <c:choose><c:when test="${pointRemainList[3]>=3}">◯</c:when>
                    	<c:otherwise>×</c:otherwise></c:choose><br>
                        ２才：　
                        <c:choose><c:when test="${pointRemainList[3]>=10}">◯</c:when>
                    	<c:otherwise>×</c:otherwise></c:choose><br>
                        １才：　
                        <c:choose><c:when test="${pointRemainList[3]>=10}">◯</c:when>
                    	<c:otherwise>×</c:otherwise></c:choose><br>
                        ０才：　
                        <c:choose><c:when test="${pointRemainList[3]>=20}">◯</c:when>
                    	<c:otherwise>×</c:otherwise></c:choose><br>
                        <br>
                    </td>

                    <td>先生：　
                    <c:choose>
                    <c:when test="${cntAll[4]/3<=cntCer[4]&&cntAll[4]>=2}">◯</c:when>
                    <c:otherwise>×</c:otherwise>
                    </c:choose>
                    <br>
                        ＜受け入れ＞<br>
                        ５才：　
                        <c:choose><c:when test="${pointRemainList[4]>=2}">◯</c:when>
                    	<c:otherwise>×</c:otherwise></c:choose><br>
                        ４才：　
                        <c:choose><c:when test="${pointRemainList[4]>=2}">◯</c:when>
                    	<c:otherwise>×</c:otherwise></c:choose><br>
                        ３才：　
                        <c:choose><c:when test="${pointRemainList[4]>=3}">◯</c:when>
                    	<c:otherwise>×</c:otherwise></c:choose><br>
                        ２才：　
                        <c:choose><c:when test="${pointRemainList[4]>=10}">◯</c:when>
                    	<c:otherwise>×</c:otherwise></c:choose><br>
                        １才：　
                        <c:choose><c:when test="${pointRemainList[4]>=10}">◯</c:when>
                    	<c:otherwise>×</c:otherwise></c:choose><br>
                        ０才：　
                        <c:choose><c:when test="${pointRemainList[4]>=20}">◯</c:when>
                    	<c:otherwise>×</c:otherwise></c:choose><br>
                        <br>
                    </td>

                    <td>先生：　
                     <c:choose>
                    <c:when test="${cntAll[5]/3<=cntCer[5]&&cntAll[5]>=2}">◯</c:when>
                    <c:otherwise>×</c:otherwise>
                    </c:choose>
                    <br>
                        ＜受け入れ＞<br>
                       ５才：　
                        <c:choose><c:when test="${pointRemainList[5]>=2}">◯</c:when>
                    	<c:otherwise>×</c:otherwise></c:choose><br>
                        ４才：　
                        <c:choose><c:when test="${pointRemainList[5]>=2}">◯</c:when>
                    	<c:otherwise>×</c:otherwise></c:choose><br>
                        ３才：　
                        <c:choose><c:when test="${pointRemainList[5]>=3}">◯</c:when>
                    	<c:otherwise>×</c:otherwise></c:choose><br>
                        ２才：　
                        <c:choose><c:when test="${pointRemainList[5]>=10}">◯</c:when>
                    	<c:otherwise>×</c:otherwise></c:choose><br>
                        １才：　
                        <c:choose><c:when test="${pointRemainList[5]>=10}">◯</c:when>
                    	<c:otherwise>×</c:otherwise></c:choose><br>
                        ０才：　
                        <c:choose><c:when test="${pointRemainList[5]>=20}">◯</c:when>
                    	<c:otherwise>×</c:otherwise></c:choose><br>
                        <br>
                    </td>

                    <td>先生：　
                     <c:choose>
                    <c:when test="${cntAll[6]/3<=cntCer[6]&&cntAll[6]>=2}">◯</c:when>
                    <c:otherwise>×</c:otherwise>
                    </c:choose>
                    <br>
                        ＜受け入れ＞<br>
                       ５才：　
                        <c:choose><c:when test="${pointRemainList[6]>=2}">◯</c:when>
                    	<c:otherwise>×</c:otherwise></c:choose><br>
                        ４才：　
                        <c:choose><c:when test="${pointRemainList[6]>=2}">◯</c:when>
                    	<c:otherwise>×</c:otherwise></c:choose><br>
                        ３才：　
                        <c:choose><c:when test="${pointRemainList[6]>=3}">◯</c:when>
                    	<c:otherwise>×</c:otherwise></c:choose><br>
                        ２才：　
                        <c:choose><c:when test="${pointRemainList[6]>=10}">◯</c:when>
                    	<c:otherwise>×</c:otherwise></c:choose><br>
                        １才：　
                        <c:choose><c:when test="${pointRemainList[6]>=10}">◯</c:when>
                    	<c:otherwise>×</c:otherwise></c:choose><br>
                        ０才：　
                        <c:choose><c:when test="${pointRemainList[6]>=20}">◯</c:when>
                    	<c:otherwise>×</c:otherwise></c:choose><br>
                        <br>
                    </td>
                    <td>先生：　
                    <c:choose>
                    <c:when test="${cntAll[7]/3<=cntCer[7]&&cntAll[7]>=2}">◯</c:when>
                    <c:otherwise>×</c:otherwise>
                    </c:choose>
                    <br>
                        ＜受け入れ＞<br>
                        ５才：　
                        <c:choose><c:when test="${pointRemainList[7]>=2}">◯</c:when>
                    	<c:otherwise>×</c:otherwise></c:choose><br>
                        ４才：　
                        <c:choose><c:when test="${pointRemainList[7]>=2}">◯</c:when>
                    	<c:otherwise>×</c:otherwise></c:choose><br>
                        ３才：　
                        <c:choose><c:when test="${pointRemainList[7]>=3}">◯</c:when>
                    	<c:otherwise>×</c:otherwise></c:choose><br>
                        ２才：　
                        <c:choose><c:when test="${pointRemainList[7]>=10}">◯</c:when>
                    	<c:otherwise>×</c:otherwise></c:choose><br>
                        １才：　
                        <c:choose><c:when test="${pointRemainList[7]>=10}">◯</c:when>
                    	<c:otherwise>×</c:otherwise></c:choose><br>
                        ０才：　
                        <c:choose><c:when test="${pointRemainList[7]>=20}">◯</c:when>
                    	<c:otherwise>×</c:otherwise></c:choose><br>
                        <br>
                    </td>

                </tr>
                <tr>
                    <td>
                        <a href="Daydetail?today=1" class="btn btn-info btn-sm">詳細</a>
                        <a href="Spot_resister?today=1" class="btn btn-info btn-sm">追加</a>
                    </td>
                    <td>
                        <a href="Daydetail?today=2" class="btn btn-info btn-sm">詳細</a>
                        <a href="Spot_resister?today=2" class="btn btn-info btn-sm">追加</a>
                    </td>
                    <td>
                    	<a href="Daydetail?today=3" class="btn btn-info btn-sm">詳細</a>
                        <a href="Spot_resister?today=3" class="btn btn-info btn-sm">追加</a>
                    </td>
                    <td>
                         <a href="Daydetail?today=4" class="btn btn-info btn-sm">詳細</a>
                        <a href="Spot_resister?today=4" class="btn btn-info btn-sm">追加</a>
                    </td>
                    <td>
                         <a href="Daydetail?today=5" class="btn btn-info btn-sm">詳細</a>
                        <a href="Spot_resister?today=5" class="btn btn-info btn-sm">追加</a>
                    </td>
                    <td>
                         <a href="Daydetail?today=6" class="btn btn-info btn-sm">詳細</a>
                        <a href="Spot_resister?today=6" class="btn btn-info btn-sm">追加</a>
                    </td>
                    <td>
                         <a href="Daydetail?today=7" class="btn btn-info btn-sm">詳細</a>
                        <a href="Spot_resister?today=7" class="btn btn-info btn-sm">追加</a>
                    </td>

                </tr>
            </tbody>
        </table>


    </div>

</body>

</html>
