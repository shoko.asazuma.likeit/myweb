<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>月極め園児一覧</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <link href="style.css" rel="stylesheet" type="text/css">
</head>


<body>
<div class="container">
    <h1 class="text-center text-dark mt-3">月極め園児一覧</h1>
            <div class="text-right">

            <a href="Kid_resister" class="btn btn-info  btn-lg mb-3 py-1">新規登録</a>
            <a href="Index" class="btn btn-info btn-lg mb-3 py-1">今週の予定</a>

        </div>

        <table class="table table-bordered">
            <thead>
                <tr>
                    <th scope="col">年齢</th>
                    <th scope="col">名前</th>
                    <th scope="col">曜日</th>
                    <th scope="col">備考</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
            <c:forEach var="kid" items="${kidList}">
                <tr>
                    <td>${kid.age}</td>
                    <td>${kid.name}</td>
                    <td>
                    <c:if test="${kid.day1==1}">月</c:if>
                    <c:if test="${kid.day2==1}">火</c:if>
                    <c:if test="${kid.day3==1}">水</c:if>
                    <c:if test="${kid.day4==1}">木</c:if>
                    <c:if test="${kid.day5==1}">金</c:if>
                    <c:if test="${kid.day6==1}">土</c:if>
                    <c:if test="${kid.day7==1}">日</c:if></td>
                    <td>${kid.memo}</td>
                    <td>
                        <a href="Kid_update?id=${kid.id}" class="btn btn-info btn-sm">更新</a>
                        <a href="Kid_delete?id=${kid.id}" class="btn btn-info btn-sm">削除</a>

                    </td>
                </tr>

                 </c:forEach>
            </tbody>
        </table>

    </div>

</body>

</html>
