<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
        <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>曜日詳細</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <link href="style.css" rel="stylesheet" type="text/css">
</head>


<body>
    <div class="container">
        <h1 class="text-center text-dark mt-3">
        <c:if test="${today==1}">月</c:if>
                    <c:if test="${today==2}">火</c:if>
                    <c:if test="${today==3}">水</c:if>
                    <c:if test="${today==4}">木</c:if>
                    <c:if test="${today==5}">金</c:if>
                    <c:if test="${today==6}">土</c:if>
                    <c:if test="${today==7}">日</c:if>
                    曜日
        </h1>
        <div class="text-right">
            <a href="Index" class="btn btn-info btn-lg mb-3 py-1">今週の予定</a>

        </div>

                    <h3 class="text-left text-dark mb-4">
                    <c:forEach var="staff" items="${staffList}">
                    ${staff.name}先生<br>
                    </c:forEach></h3>



        <table class="table table-bordered mb-4" style="table-layout:fixed;width:100%;">

            <thead class="thead-light">
                <tr>
                    <th scope="col">年齢</th>
                    <th scope="col">名前</th>
                    <th scope="col">人数</th>
                </tr>
            </thead>
            <tbody>

                <tr>
                    <td>０才</td>
                    <td>
                    <c:forEach var="mkid" items="${mkidList}">
                    <c:if test="${mkid.age==0}">${mkid.name}<br>
                    </c:if>
                    </c:forEach>
                    </td>
                    <td>
                    <c:forEach var="agecnt" items="${agecntList}">
                    <c:if test="${agecnt.age==0}">${agecnt.cnt}人
                    </c:if>
                    </c:forEach>
                    <c:forEach var="agecnt" items="${dagecntList}">
                    <c:if test="${agecnt.age==0}">(+${agecnt.cnt}人)
                    </c:if>
                    </c:forEach>
                    </td>

                </tr>
                <tr>
                    <td>１才</td>
                    <td>
                    <c:forEach var="mkid" items="${mkidList}">
                    <c:if test="${mkid.age==1}">${mkid.name}<br>
                    </c:if>
                    </c:forEach>
                    </td>
                    <td>
                    <c:forEach var="agecnt" items="${agecntList}">
                    <c:if test="${agecnt.age==1}">${agecnt.cnt}人
                    </c:if>
                    </c:forEach>

                   <c:forEach var="agecnt" items="${dagecntList}">
                    <c:if test="${agecnt.age==1}">(+${agecnt.cnt}人)
                    </c:if>
                    </c:forEach>

                    </td>
                </tr>

                <tr>
                    <td>２才</td>


                    <td>
                    <c:forEach var="mkid" items="${mkidList}">
                    <c:if test="${mkid.age==2}">${mkid.name}<br>
                    </c:if>
                    </c:forEach>
                    </td>
                    <td>
                     <c:forEach var="agecnt" items="${agecntList}">
                    <c:if test="${agecnt.age==2}">${agecnt.cnt}人
                    </c:if>
                    </c:forEach>

                    <c:forEach var="agecnt" items="${dagecntList}">
                    <c:if test="${agecnt.age==2}">(+${agecnt.cnt}人)
                    </c:if>
                    </c:forEach>

                    </td>
                </tr>
                <tr>
                    <td>３才</td>


                    <td>
                    <c:forEach var="mkid" items="${mkidList}">
                    <c:if test="${mkid.age==3}">${mkid.name}<br>
                    </c:if>
                    </c:forEach>

                    </td>
                    <td><c:forEach var="agecnt" items="${agecntList}">
                    <c:if test="${agecnt.age==3}">${agecnt.cnt}人
                    </c:if>
                    </c:forEach>

                    <c:forEach var="agecnt" items="${dagecntList}">
                    <c:if test="${agecnt.age==3}">(+${agecnt.cnt}人)
                    </c:if>
                    </c:forEach>

                    </td>

                </tr>
                <tr>
                    <td>４才</td>

                    <td> <c:forEach var="mkid" items="${mkidList}">
                    <c:if test="${mkid.age==4}">${mkid.name}<br>
                    </c:if>
                    </c:forEach>
                    </td>
                    <td><c:forEach var="agecnt" items="${agecntList}">
                    <c:if test="${agecnt.age==4}">${agecnt.cnt}人
                    </c:if>
                    </c:forEach>

                    <c:forEach var="agecnt" items="${dagecntList}">
                    <c:if test="${agecnt.age==4}">(+${agecnt.cnt}人)
                    </c:if>
                    </c:forEach>

                    </td>
                </tr>
                <tr>
                    <td>５才</td>


                    <td> <c:forEach var="mkid" items="${mkidList}">
                    <c:if test="${mkid.age==5}">${mkid.name}<br>
                    </c:if>
                    </c:forEach>

                    </td>
                    <td><c:forEach var="agecnt" items="${agecntList}">
                    <c:if test="${agecnt.age==5}">${agecnt.cnt}人
                    </c:if>
                    </c:forEach>

                    <c:forEach var="agecnt" items="${dagecntList}">
                    <c:if test="${agecnt.age==5}">(+${agecnt.cnt}人)
                    </c:if>
                    </c:forEach>

                    </td>
                </tr>


            </tbody>
        </table>

        <h3 class="text-left text-dark mb-1">一時預かり</h3>
        <table class="table table-bordered mb-4" style="table-layout:fixed;width:100%;">

            <thead class="thead-light">
                <tr>
                    <th scope="col">年齢</th>
                    <th scope="col">名前</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
            <c:forEach var="dkid" items="${dkidList}">
                <tr>
                    <td>${dkid.age}</td>
                    <td>${dkid.name}</td>
                    <td><a href="Spot_delete?id=${dkid.id}" class="btn btn-info btn-sm">予約取消</a>
                    </td>
                </tr>
                </c:forEach>

            </tbody>
        </table>


    </div>

</body>

</html>
