
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
        <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
        <!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>月極め園児削除画面</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <link href="style.css" rel="stylesheet" type="text/css">
</head>

<body>

    <h1 class="text-center text-dark mt-3 mb-3">削除</h1>
    <div class="deletecontainer">
        <p>名前：${kid.name}</p>
        <p>削除してもよろしいですか</p>

<form action="Kid_delete" method="post">
        <a href="Kid_list" type="button" class="btn btn-info">キャンセル</a>
        <input type="hidden" value="${kid.id}" name="id">
        <button type="submit" class="btn btn-info float-right">　 削除 　</button>

</form>
    </div>
</body>

</html>
