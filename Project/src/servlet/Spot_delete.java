package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Daily_kidBeans;
import dao.Daily_kidDao;

/**
 * Servlet implementation class Spot_delete
 */
@WebServlet("/Spot_delete")
public class Spot_delete extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Spot_delete() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameter("id");

		Daily_kidBeans kid=Daily_kidDao.findById(Integer.parseInt(id));
		request.setAttribute("kid",kid);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/spot_delete.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("utf-8");
		String id = request.getParameter("id");

		int rs=Daily_kidDao.delete(id);
		if(rs>0) {
			response.sendRedirect("Index");
		}else {

			//失敗時の処理考える
			response.sendRedirect("Index");
	}
	}
}

