package servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Monthly_kidBeans;
import dao.Monthly_kidDao;

/**
 * Servlet implementation class Kid_list
 */
@WebServlet("/Kid_list")
public class Kid_list extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Kid_list() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 List<Monthly_kidBeans> kidList= new ArrayList<Monthly_kidBeans>();
		kidList=Monthly_kidDao.findAll();
		request.setAttribute("kidList", kidList);

		/*for(Monthly_kidBeans kid:kidList) {
		System.out.println(kid.getName());}
		 */


		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/kid_list.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
