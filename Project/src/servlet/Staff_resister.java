package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.StaffBeans;
import dao.StaffDao;

/**
 * Servlet implementation class Staff_resister
 */
@WebServlet("/Staff_resister")
public class Staff_resister extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Staff_resister() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServ
	 * let#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/staff_resister.jsp");
		dispatcher.forward(request, response);
	}


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		String name=request.getParameter("name");
		String certificate=request.getParameter("certificate");
		String day1=request.getParameter("day1");
		String day2=request.getParameter("day2");
		String day3=request.getParameter("day3");
		String day4=request.getParameter("day4");
		String day5=request.getParameter("day5");
		String day6=request.getParameter("day6");
		String day7=request.getParameter("day7");
		String memo=request.getParameter("memo");


		StaffBeans staff = new StaffBeans();
		staff.setName(name);
		staff.setCertificate(Integer.parseInt(certificate));
		staff.setDay1(Integer.parseInt(day1));
		staff.setDay2(Integer.parseInt(day2));
		staff.setDay3(Integer.parseInt(day3));
		staff.setDay4(Integer.parseInt(day4));
		staff.setDay5(Integer.parseInt(day5));
		staff.setDay6(Integer.parseInt(day6));
		staff.setDay7(Integer.parseInt(day7));
		staff.setMemo(memo);


		int i=StaffDao.resisterStaff(staff);
		System.out.println(i);

		response.sendRedirect("Index");
	}

}
