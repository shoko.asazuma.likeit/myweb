package servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Agecnt;
import beans.Daily_kidBeans;
import beans.Monthly_kidBeans;
import beans.StaffBeans;
import dao.Daily_kidDao;
import dao.Monthly_kidDao;
import dao.StaffDao;

/**
 * Servlet implementation class Daydetail
 */
@WebServlet("/Daydetail")
public class Daydetail extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Daydetail() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		String today=request.getParameter("today");
		request.setAttribute("today",today);


		List<StaffBeans> staffList= new ArrayList<StaffBeans>();
		staffList=StaffDao.findByDay(Integer.parseInt(today));
		request.setAttribute("staffList", staffList);
		
		
		
		List<Monthly_kidBeans> mkidList= new ArrayList<Monthly_kidBeans>();
		mkidList=Monthly_kidDao.findByDay(Integer.parseInt(today));
		request.setAttribute("mkidList", mkidList);
		
		
		List<Daily_kidBeans> dkidList= new ArrayList<Daily_kidBeans>();
		dkidList=Daily_kidDao.findByDay(Integer.parseInt(today));
		request.setAttribute("dkidList", dkidList);
		

		List<Agecnt> agecntList= new ArrayList<Agecnt>();
		agecntList=Monthly_kidDao.countByDay(Integer.parseInt(today));
		request.setAttribute("agecntList", agecntList);
		
		List<Agecnt> dagecntList= new ArrayList<Agecnt>();
		dagecntList=Daily_kidDao.countByDay(Integer.parseInt(today));
		request.setAttribute("dagecntList", dagecntList);
		

		
		
		for(Agecnt agecnt:dagecntList) {
		System.out.println(agecnt.getCnt());}

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/daydetail.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
