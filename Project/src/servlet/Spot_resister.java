package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Daily_kidBeans;
import dao.Daily_kidDao;

/**
 * Servlet implementation class Spot_resister
 */
@WebServlet("/Spot_resister")
public class Spot_resister extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Spot_resister() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String day = request.getParameter("today");
		request.setAttribute("day",day);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/spot_resister.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		String name=request.getParameter("name");
		String age=request.getParameter("age");
		String day=request.getParameter("today");
	

		Daily_kidBeans kid = new Daily_kidBeans();
		kid.setName(name);
		kid.setAge(Integer.parseInt(age));
		kid.setDay(Integer.parseInt(day));


		int i=Daily_kidDao.resisterDaily_kid(kid);
		System.out.println(i);

		response.sendRedirect("Index");
	}

}
