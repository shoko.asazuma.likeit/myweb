package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Monthly_kidBeans;
import dao.Monthly_kidDao;

/**
 * Servlet implementation class Kid_update
 */
@WebServlet("/Kid_update")
public class Kid_update extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Kid_update() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameter("id");
		
		Monthly_kidBeans kid=Monthly_kidDao.findById(Integer.parseInt(id));
		

		request.setAttribute("kid",kid);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/kid_update.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		String id=request.getParameter("id");
		String name=request.getParameter("name");
		String age=request.getParameter("age");
		String day1=request.getParameter("day1");
		String day2=request.getParameter("day2");
		String day3=request.getParameter("day3");
		String day4=request.getParameter("day4");
		String day5=request.getParameter("day5");
		String day6=request.getParameter("day6");
		String day7=request.getParameter("day7");
		String memo=request.getParameter("memo");


		Monthly_kidBeans kid = new Monthly_kidBeans();
		kid.setId(Integer.parseInt(id));
		kid.setName(name);
		kid.setAge(Integer.parseInt(age));
		kid.setDay1(Integer.parseInt(day1));
		kid.setDay2(Integer.parseInt(day2));
		kid.setDay3(Integer.parseInt(day3));
		kid.setDay4(Integer.parseInt(day4));
		kid.setDay5(Integer.parseInt(day5));
		kid.setDay6(Integer.parseInt(day6));
		kid.setDay7(Integer.parseInt(day7));
		kid.setMemo(memo);


		int i=Monthly_kidDao.updateKid(kid);
		System.out.println(i);

		response.sendRedirect("Kid_list");
	}

}
