package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.Daily_kidDao;
import dao.Monthly_kidDao;
import dao.StaffDao;

/**
 * Servlet implementation class Index
 */
@WebServlet("/Index")
public class Index extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Index() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		int[] cntCer = new int[8];

		for(int i=1;i<8;i++) {
		int cntday = StaffDao.countCertificateByDay(i);
		cntCer[i]= cntday;
		}

		int[] cntAll = new int[8];

		for(int i=1;i<8;i++) {
		int cntday = StaffDao.countAllByDay(i);
		cntAll[i]= cntday;
		}

		int[] pointRemainList = new int[8];

		for(int i=1;i<8;i++) {
			int age0=Monthly_kidDao.countByDayAndAge(i, 0)+Daily_kidDao.countByDayAndAge(i, 0);
			int age1or2=Monthly_kidDao.countByDayAndAge(i, 1)+Monthly_kidDao.countByDayAndAge(i, 2)
			+Daily_kidDao.countByDayAndAge(i, 1)+Daily_kidDao.countByDayAndAge(i, 2);
			int age3=Monthly_kidDao.countByDayAndAge(i, 3)+Daily_kidDao.countByDayAndAge(i, 3);
			int age4or5=Monthly_kidDao.countByDayAndAge(i, 4)+Monthly_kidDao.countByDayAndAge(i, 5)
			+Daily_kidDao.countByDayAndAge(i, 4)+Daily_kidDao.countByDayAndAge(i, 5);
			int staff=StaffDao.countAllByDay(i);

			int pointRemain=
			staff*60
			-age0*20
			-age1or2*10
			-age3*3
			-age4or5*2;

			pointRemainList[i]=pointRemain;
		}





		for(int i=1;i<8;i++) {
			System.out.println(pointRemainList[i]);
		}

		request.setAttribute("cntCer", cntCer);
		request.setAttribute("cntAll", cntAll);
		request.setAttribute("pointRemainList", pointRemainList);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/index.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

