package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.StaffBeans;
import dao.StaffDao;

/**
 * Servlet implementation class Staff_delete
 */
@WebServlet("/Staff_delete")
public class Staff_delete extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Staff_delete() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameter("id");
		
		StaffBeans staff=StaffDao.findById(Integer.parseInt(id));
		

		request.setAttribute("staff",staff);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/staff_delete.jsp");
		dispatcher.forward(request, response);
	}


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("utf-8");
		String id = request.getParameter("id");

		int rs=StaffDao.delete(id);
		if(rs>0) {
			response.sendRedirect("Staff_list");
		}else {

			//失敗時の処理考える
			response.sendRedirect("Staff_delete");
		}
	}

}
