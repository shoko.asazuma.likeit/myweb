
package servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.StaffBeans;
import dao.StaffDao;

/**
 * Servlet implementation class Staff_list
 */
@WebServlet("/Staff_list")
public class Staff_list extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
	
    public Staff_list() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		List<StaffBeans> staffList= new ArrayList<StaffBeans>();
		staffList=StaffDao.findAll();
		request.setAttribute("staffList", staffList);
		
		/*
		for(StaffBeans staff:staffList) {
		System.out.println(staff.getName());}
		*/
	
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/staff_list.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
