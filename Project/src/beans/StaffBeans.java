package beans;
import java.io.Serializable;
public class StaffBeans implements Serializable {
	private int id;
	private String name;
	private int day1;
	private int day2;
	private int day3;
	private int day4;
	private int day5;
	private int day6;
	private int day7;
	private int certificate;
	private String memo;
	
	
	public StaffBeans() {
		// TODO 自動生成されたコンストラクター・スタブ
	}


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getDay1() {
		return day1;
	}
	public void setDay1(int day1) {
		this.day1 = day1;
	}
	public int getDay2() {
		return day2;
	}
	public void setDay2(int day2) {
		this.day2 = day2;
	}
	public int getDay3() {
		return day3;
	}
	public void setDay3(int day3) {
		this.day3 = day3;
	}
	public int getDay4() {
		return day4;
	}
	public void setDay4(int day4) {
		this.day4 = day4;
	}
	public int getDay5() {
		return day5;
	}
	public void setDay5(int day5) {
		this.day5 = day5;
	}
	public int getDay6() {
		return day6;
	}
	public void setDay6(int day6) {
		this.day6 = day6;
	}
	public int getDay7() {
		return day7;
	}
	public void setDay7(int day7) {
		this.day7 = day7;
	}
	public int getCertificate() {
		return certificate;
	}
	public void setCertificate(int certificate) {
		this.certificate = certificate;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	
	


	
}
