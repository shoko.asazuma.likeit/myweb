package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.StaffBeans;


public class StaffDao {


	public static int resisterStaff(StaffBeans staff) {
		Connection con = null;
		PreparedStatement st = null;
		int result = 0;
		try {
            // データベースへ接続
        	con = DaoManager.getConnection();

        	 st=con.prepareStatement("insert into staff values (0,?,?,?,?,?,?,?,?,?,?)");

              st.setString(1, staff.getName());
              st.setInt(2, staff.getDay1());
              st.setInt(3, staff.getDay2());
              st.setInt(4, staff.getDay3());
              st.setInt(5, staff.getDay4());
              st.setInt(6, staff.getDay5());
              st.setInt(7, staff.getDay6());
              st.setInt(8, staff.getDay7());
              st.setInt(9, staff.getCertificate());
              st.setString(10, staff.getMemo());

 			result = st.executeUpdate();

	} catch (SQLException  e) {
		e.printStackTrace();
		//System.out.println("データの登録に失敗しました。");
	} finally {
		try {
			if (st != null) {
				st.close();
			}
			if (con != null) {
				con.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
        	return result;
	}

	public static List<StaffBeans> findAll() {
        Connection con = null;
        PreparedStatement stmt =null;
        StaffBeans staff=null;
        List<StaffBeans> staffList= new ArrayList<StaffBeans>();
        try {

            con = DaoManager.getConnection();

            String sql = "SELECT * FROM staff";

            stmt = con.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();

            while(rs.next()) {
            	staff=new StaffBeans();
            	staff.setId(rs.getInt("id"));
            	staff.setName(rs.getString("name"));
            	staff.setDay1(rs.getInt("day1"));
            	staff.setDay2(rs.getInt("day2"));
            	staff.setDay3(rs.getInt("day3"));
            	staff.setDay4(rs.getInt("day4"));
            	staff.setDay5(rs.getInt("day5"));
            	staff.setDay6(rs.getInt("day6"));
            	staff.setDay7(rs.getInt("day7"));
            	staff.setCertificate(rs.getInt("certificate"));
            	staff.setMemo(rs.getString("memo"));

            	staffList.add(staff);
            	}
            stmt.close();
        } catch (SQLException e) {
        	e.printStackTrace();
        	return null;
        }
        finally {
        	if (con != null) {
        		try {
        			con.close();
        		} catch (SQLException e) {
        			e.printStackTrace();
        			return null;
        		}
        	}
        }
        return staffList;
    }

	public static StaffBeans findById(int id) {
        Connection con = null;
        PreparedStatement stmt =null;
        StaffBeans staff=new StaffBeans();
        try {

            con = DaoManager.getConnection();

            String sql = "SELECT * FROM staff where id=? ";

            stmt = con.prepareStatement(sql);
            stmt.setInt(1, id);

            ResultSet rs = stmt.executeQuery();

            if(rs.next()) {
            	staff.setId(rs.getInt("id"));
            	staff.setName(rs.getString("name"));
            	staff.setDay1(rs.getInt("day1"));
            	staff.setDay2(rs.getInt("day2"));
            	staff.setDay3(rs.getInt("day3"));
            	staff.setDay4(rs.getInt("day4"));
            	staff.setDay5(rs.getInt("day5"));
            	staff.setDay6(rs.getInt("day6"));
            	staff.setDay7(rs.getInt("day7"));
            	staff.setCertificate(rs.getInt("certificate"));
            	staff.setMemo(rs.getString("memo"));
            	}
            stmt.close();
        } catch (SQLException e) {
        	e.printStackTrace();
        	return null;
        }
        finally {
        	if (con != null) {
        		try {
        			con.close();
        		} catch (SQLException e) {
        			e.printStackTrace();
        			return null;
        		}
        	}
        }
        return staff;
    }

	public static  int delete(String id) {
        Connection con = null;
        PreparedStatement st = null;
        int result = 0;
        try {
            // データベースへ接続
        	con = DaoManager.getConnection();

        	 st=con.prepareStatement("DELETE FROM staff WHERE id = ?");
 			st.setString(1, id);

 			result = st.executeUpdate();

	} catch (SQLException e) {
		e.printStackTrace();

	} finally {
		try {
			if (st != null) {
				st.close();
			}
			if (con != null) {
				con.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
        	return result;
    }

	public static int updateStaff(StaffBeans staff) {
		Connection con = null;
		PreparedStatement st = null;
		int result = 0;
		try {
            // データベースへ接続
        	con = DaoManager.getConnection();

        	 st=con.prepareStatement("UPDATE staff SET name=?,day1=?,day2=?,day3=?,day4=?,day5=?,day6=?,day7=?,certificate=?, memo=? where id=?");


              st.setString(1, staff.getName());
              st.setInt(2, staff.getDay1());
              st.setInt(3, staff.getDay2());
              st.setInt(4, staff.getDay3());
              st.setInt(5, staff.getDay4());
              st.setInt(6, staff.getDay5());
              st.setInt(7, staff.getDay6());
              st.setInt(8, staff.getDay7());
              st.setInt(9, staff.getCertificate());
              st.setString(10, staff.getMemo());
              st.setInt(11, staff.getId());

 			result = st.executeUpdate();

	} catch (SQLException  e) {
		e.printStackTrace();
		System.out.println("データの登録に失敗しました。");
	} finally {
		try {
			if (st != null) {
				st.close();
			}
			if (con != null) {
				con.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
        	return result;
	}


	public static List<StaffBeans> findByDay(int day) {
        Connection con = null;
        PreparedStatement stmt =null;
        StaffBeans staff=null;
        List<StaffBeans> staffList= new ArrayList<StaffBeans>();
        try {

            con = DaoManager.getConnection();

            String sql = "SELECT * FROM staff where day?=1 ";

            stmt = con.prepareStatement(sql);
            stmt.setInt(1, day);

            ResultSet rs = stmt.executeQuery();

            while(rs.next()) {
            	staff=new StaffBeans();
            	staff.setId(rs.getInt("id"));
            	staff.setName(rs.getString("name"));
            	staff.setDay1(rs.getInt("day1"));
            	staff.setDay2(rs.getInt("day2"));
            	staff.setDay3(rs.getInt("day3"));
            	staff.setDay4(rs.getInt("day4"));
            	staff.setDay5(rs.getInt("day5"));
            	staff.setDay6(rs.getInt("day6"));
            	staff.setDay7(rs.getInt("day7"));
            	staff.setCertificate(rs.getInt("certificate"));
            	staff.setMemo(rs.getString("memo"));

            	staffList.add(staff);
            	}
            stmt.close();
        } catch (SQLException e) {
        	e.printStackTrace();
        	return null;
        }
        finally {
        	if (con != null) {
        		try {
        			con.close();
        		} catch (SQLException e) {
        			e.printStackTrace();
        			return null;
        		}
        	}
        }
        return staffList;
    }


	public static int countAllByDay(int day) {
        Connection con = null;
        PreparedStatement stmt =null;
        int cnt=0;
        try {

            con = DaoManager.getConnection();

            String sql = "SELECT COUNT(*) as cnt FROM staff where day?=1";

            stmt = con.prepareStatement(sql);
            stmt.setInt(1, day);

            ResultSet rs = stmt.executeQuery();
            if(rs.next()) {
            cnt=rs.getInt("cnt");
            }
            stmt.close();
        } catch (SQLException  e) {
    		e.printStackTrace();
    		System.out.println("データの登録に失敗しました。");
    	} finally {
    		try {
    			if (stmt != null) {
    				stmt.close();
    			}
    			if (con != null) {
    				con.close();
    			}
    		} catch (SQLException e) {
    			e.printStackTrace();
    		}
    	}
        return cnt;
    }

	public static int countCertificateByDay(int day) {
        Connection con = null;
        PreparedStatement stmt =null;
        int cnt=0;
        try {

            con = DaoManager.getConnection();

            String sql = "SELECT COUNT(*) as cnt FROM staff where day?=1 and certificate=1";

            stmt = con.prepareStatement(sql);
            stmt.setInt(1, day);

            ResultSet rs = stmt.executeQuery();
            if(rs.next()) {
            cnt=rs.getInt("cnt");
            }
            stmt.close();
        } catch (SQLException  e) {
    		e.printStackTrace();
    		System.out.println("データの登録に失敗しました。");
    	} finally {
    		try {
    			if (stmt != null) {
    				stmt.close();
    			}
    			if (con != null) {
    				con.close();
    			}
    		} catch (SQLException e) {
    			e.printStackTrace();
    		}
    	}
        return cnt;
    }


}
