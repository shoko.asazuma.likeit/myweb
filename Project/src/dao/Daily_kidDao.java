package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.Agecnt;
import beans.Daily_kidBeans;


public class Daily_kidDao {


	public static int resisterDaily_kid(Daily_kidBeans kid) {
		Connection con = null;
		PreparedStatement st = null;
		int result = 0;
		try {
            // データベースへ接続
        	con = DaoManager.getConnection();

        	 st=con.prepareStatement("insert into daily_kid values (0,?,?,?,null)");

              st.setString(1, kid.getName());
              st.setInt(2, kid.getDay());
              st.setInt(3, kid.getAge());

 			result = st.executeUpdate();

	} catch (SQLException  e) {
		e.printStackTrace();
		//System.out.println("データの登録に失敗しました。");
	} finally {
		try {
			if (st != null) {
				st.close();
			}
			if (con != null) {
				con.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
        	return result;
	}

	public static Daily_kidBeans findById(int id) {
        Connection con = null;
        PreparedStatement stmt =null;
        Daily_kidBeans kid=new Daily_kidBeans();
        try {

            con = DaoManager.getConnection();

            String sql = "SELECT * FROM daily_kid where id=? ";

            stmt = con.prepareStatement(sql);
            stmt.setInt(1, id);

            ResultSet rs = stmt.executeQuery();

            if(rs.next()) {
            	kid.setId(rs.getInt("id"));
            	kid.setName(rs.getString("name"));
            	kid.setDay(rs.getInt("day"));
            	kid.setAge(rs.getInt("age"));
            	kid.setMemo(rs.getString("memo"));
            	}
            stmt.close();
        } catch (SQLException e) {
        	e.printStackTrace();
        	return null;
        }
        finally {
        	if (con != null) {
        		try {
        			con.close();
        		} catch (SQLException e) {
        			e.printStackTrace();
        			return null;
        		}
        	}
        }
        return kid;
    }

	public static  int delete(String id) {
        Connection con = null;
        PreparedStatement st = null;
        int result = 0;
        try {
            // データベースへ接続
        	con = DaoManager.getConnection();

        	 st=con.prepareStatement("DELETE FROM daily_kid WHERE id = ?");
 			st.setString(1, id);

 			result = st.executeUpdate();

	} catch (SQLException e) {
		e.printStackTrace();

	} finally {
		try {
			if (st != null) {
				st.close();
			}
			if (con != null) {
				con.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
        	return result;
    }

	public static List<Daily_kidBeans> findByDay(int day) {
        Connection con = null;
        PreparedStatement stmt =null;
        Daily_kidBeans kid=null;
        List<Daily_kidBeans> kidList= new ArrayList<Daily_kidBeans>();
        try {

            con = DaoManager.getConnection();

            String sql = "SELECT * FROM daily_kid where day=?";

            stmt = con.prepareStatement(sql);
            stmt.setInt(1, day);

            ResultSet rs = stmt.executeQuery();

            while(rs.next()) {
            	kid=new Daily_kidBeans();
            	kid.setId(rs.getInt("id"));
            	kid.setName(rs.getString("name"));
            	kid.setDay(rs.getInt("day"));
            	kid.setAge(rs.getInt("age"));
            	kid.setMemo(rs.getString("memo"));

            	kidList.add(kid);
            	}
            stmt.close();
        } catch (SQLException e) {
        	e.printStackTrace();
        	return null;
        }
        finally {
        	if (con != null) {
        		try {
        			con.close();
        		} catch (SQLException e) {
        			e.printStackTrace();
        			return null;
        		}
        	}
        }
        return kidList;
    }

	public static List<Agecnt> countByDay(int day) {
        Connection con = null;
        PreparedStatement stmt =null;
        List<Agecnt> agecntList= new ArrayList<Agecnt>();
        Agecnt agecnt=null;

        try {

            con = DaoManager.getConnection();

            String sql = "SELECT age, COUNT(age) as cnt FROM daily_kid where day=? GROUP BY age";

            stmt = con.prepareStatement(sql);
            stmt.setInt(1, day);


            ResultSet rs = stmt.executeQuery();

            while(rs.next()) {
            	agecnt=new Agecnt();
            	agecnt.setAge(rs.getInt("age"));
            	agecnt.setCnt(rs.getInt("cnt"));

            	agecntList.add(agecnt);
            	}
            stmt.close();
        } catch (SQLException e) {
        	e.printStackTrace();
        	return null;
        }
        finally {
        	if (con != null) {
        		try {
        			con.close();
        		} catch (SQLException e) {
        			e.printStackTrace();
        			return null;
        		}
        	}
        }
        return agecntList;
    }

	public static int countByDayAndAge(int day,int age) {
        Connection con = null;
        PreparedStatement stmt =null;
        int cnt=0;

        try {

            con = DaoManager.getConnection();

            String sql = "SELECT COUNT(*) as cnt FROM daily_kid where day=? and age=?";

            stmt = con.prepareStatement(sql);
            stmt.setInt(1, day);
            stmt.setInt(2, age);


            ResultSet rs = stmt.executeQuery();

            while(rs.next()) {
            	cnt=rs.getInt("cnt");
            	}
            stmt.close();
        } catch (SQLException e) {
        	e.printStackTrace();
    		System.out.println("データの登録に失敗しました。");
    	} finally {
    		try {
    			if (stmt != null) {
    				stmt.close();
    			}
    			if (con != null) {
    				con.close();
    			}
    		} catch (SQLException e) {
    			e.printStackTrace();
    		}

        }
        return cnt;
    }



}
