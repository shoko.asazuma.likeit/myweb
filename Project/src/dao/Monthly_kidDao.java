package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.Agecnt;
import beans.Monthly_kidBeans;


public class Monthly_kidDao {


	public static int resisterMonthly_kid(Monthly_kidBeans kid) {
		Connection con = null;
		PreparedStatement st = null;
		int result = 0;
		try {
            // データベースへ接続
        	con = DaoManager.getConnection();

        	 st=con.prepareStatement("insert into monthly_kid values (0,?,?,?,?,?,?,?,?,?,?)");

              st.setString(1, kid.getName());
              st.setInt(2, kid.getDay1());
              st.setInt(3, kid.getDay2());
              st.setInt(4, kid.getDay3());
              st.setInt(5, kid.getDay4());
              st.setInt(6, kid.getDay5());
              st.setInt(7, kid.getDay6());
              st.setInt(8, kid.getDay7());
              st.setInt(9, kid.getAge());
              st.setString(10, kid.getMemo());

 			result = st.executeUpdate();

	} catch (SQLException  e) {
		e.printStackTrace();
		//System.out.println("データの登録に失敗しました。");
	} finally {
		try {
			if (st != null) {
				st.close();
			}
			if (con != null) {
				con.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
        	return result;
	}



	public static List<Monthly_kidBeans> findAll() {
        Connection con = null;
        PreparedStatement stmt =null;
        Monthly_kidBeans kid=null;
        List<Monthly_kidBeans> kidList= new ArrayList<Monthly_kidBeans>();
        try {

            con = DaoManager.getConnection();

            String sql = "SELECT * FROM monthly_kid";

            stmt = con.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();

            while(rs.next()) {
            	kid=new Monthly_kidBeans();
            	kid.setId(rs.getInt("id"));
            	kid.setName(rs.getString("name"));
            	kid.setDay1(rs.getInt("day1"));
            	kid.setDay2(rs.getInt("day2"));
            	kid.setDay3(rs.getInt("day3"));
            	kid.setDay4(rs.getInt("day4"));
            	kid.setDay5(rs.getInt("day5"));
            	kid.setDay6(rs.getInt("day6"));
            	kid.setDay7(rs.getInt("day7"));
            	kid.setAge(rs.getInt("age"));
            	kid.setMemo(rs.getString("memo"));

            	kidList.add(kid);
            	}
            stmt.close();
        } catch (SQLException e) {
        	e.printStackTrace();
        	return null;
        }
        finally {
        	if (con != null) {
        		try {
        			con.close();
        		} catch (SQLException e) {
        			e.printStackTrace();
        			return null;
        		}
        	}
        }
        return kidList;
    }



	public static Monthly_kidBeans findById(int id) {
        Connection con = null;
        PreparedStatement stmt =null;
        Monthly_kidBeans kid=new Monthly_kidBeans();
        try {

            con = DaoManager.getConnection();

            String sql = "SELECT * FROM monthly_kid where id=? ";

            stmt = con.prepareStatement(sql);
            stmt.setInt(1, id);

            ResultSet rs = stmt.executeQuery();

            if(rs.next()) {
            	kid.setId(rs.getInt("id"));
            	kid.setName(rs.getString("name"));
            	kid.setDay1(rs.getInt("day1"));
            	kid.setDay2(rs.getInt("day2"));
            	kid.setDay3(rs.getInt("day3"));
            	kid.setDay4(rs.getInt("day4"));
            	kid.setDay5(rs.getInt("day5"));
            	kid.setDay6(rs.getInt("day6"));
            	kid.setDay7(rs.getInt("day7"));
            	kid.setAge(rs.getInt("age"));
            	kid.setMemo(rs.getString("memo"));
            	}
            stmt.close();
        } catch (SQLException e) {
        	e.printStackTrace();
        	return null;
        }
        finally {
        	if (con != null) {
        		try {
        			con.close();
        		} catch (SQLException e) {
        			e.printStackTrace();
        			return null;
        		}
        	}
        }
        return kid;
    }

	public static  int delete(String id) {
        Connection con = null;
        PreparedStatement st = null;
        int result = 0;
        try {
            // データベースへ接続
        	con = DaoManager.getConnection();

        	 st=con.prepareStatement("DELETE FROM monthly_kid WHERE id = ?");
 			st.setString(1, id);

 			result = st.executeUpdate();

	} catch (SQLException e) {
		e.printStackTrace();

	} finally {
		try {
			if (st != null) {
				st.close();
			}
			if (con != null) {
				con.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
        	return result;
    }

	public static int updateKid(Monthly_kidBeans kid) {
		Connection con = null;
		PreparedStatement st = null;
		int result = 0;
		try {
            // データベースへ接続
        	con = DaoManager.getConnection();

        	 st=con.prepareStatement("UPDATE monthly_kid SET name=?,day1=?,day2=?,day3=?,day4=?,day5=?,day6=?,day7=?,age=?, memo=? where id=?");


              st.setString(1, kid.getName());
              st.setInt(2, kid.getDay1());
              st.setInt(3, kid.getDay2());
              st.setInt(4, kid.getDay3());
              st.setInt(5, kid.getDay4());
              st.setInt(6, kid.getDay5());
              st.setInt(7, kid.getDay6());
              st.setInt(8, kid.getDay7());
              st.setInt(9, kid.getAge());
              st.setString(10, kid.getMemo());
              st.setInt(11, kid.getId());

 			result = st.executeUpdate();

	} catch (SQLException  e) {
		e.printStackTrace();
		System.out.println("データの登録に失敗しました。");
	} finally {
		try {
			if (st != null) {
				st.close();
			}
			if (con != null) {
				con.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
        	return result;
	}

	public static List<Monthly_kidBeans> findByDay(int day) {
        Connection con = null;
        PreparedStatement stmt =null;
        Monthly_kidBeans kid=null;
        List<Monthly_kidBeans> kidList= new ArrayList<Monthly_kidBeans>();
        try {

            con = DaoManager.getConnection();

            String sql = "SELECT * FROM monthly_kid where day?=1";

            stmt = con.prepareStatement(sql);
            stmt.setInt(1, day);


            ResultSet rs = stmt.executeQuery();

            while(rs.next()) {
            	kid=new Monthly_kidBeans();
            	kid.setId(rs.getInt("id"));
            	kid.setName(rs.getString("name"));
            	kid.setDay1(rs.getInt("day1"));
            	kid.setDay2(rs.getInt("day2"));
            	kid.setDay3(rs.getInt("day3"));
            	kid.setDay4(rs.getInt("day4"));
            	kid.setDay5(rs.getInt("day5"));
            	kid.setDay6(rs.getInt("day6"));
            	kid.setDay7(rs.getInt("day7"));
            	kid.setAge(rs.getInt("age"));
            	kid.setMemo(rs.getString("memo"));

            	kidList.add(kid);
            	}
            stmt.close();
        } catch (SQLException e) {
        	e.printStackTrace();
        	return null;
        }
        finally {
        	if (con != null) {
        		try {
        			con.close();
        		} catch (SQLException e) {
        			e.printStackTrace();
        			return null;
        		}
        	}
        }
        return kidList;
    }

	public static List<Agecnt> countByDay(int day) {
        Connection con = null;
        PreparedStatement stmt =null;
        List<Agecnt> agecntList= new ArrayList<Agecnt>();
        Agecnt agecnt=null;

        try {

            con = DaoManager.getConnection();

            String sql = "SELECT age, COUNT(age) as cnt FROM monthly_kid where day?=1 GROUP BY age";

            stmt = con.prepareStatement(sql);
            stmt.setInt(1, day);


            ResultSet rs = stmt.executeQuery();

            while(rs.next()) {
            	agecnt=new Agecnt();
            	agecnt.setAge(rs.getInt("age"));
            	agecnt.setCnt(rs.getInt("cnt"));

            	agecntList.add(agecnt);
            	}
            stmt.close();
        } catch (SQLException e) {
        	e.printStackTrace();
        	return null;
        }
        finally {
        	if (con != null) {
        		try {
        			con.close();
        		} catch (SQLException e) {
        			e.printStackTrace();
        			return null;
        		}
        	}
        }
        return agecntList;
    }



	public static int countByDayAndAge(int day,int age) {
        Connection con = null;
        PreparedStatement stmt =null;
        int cnt=0;

        try {

            con = DaoManager.getConnection();

            String sql = "SELECT COUNT(*) as cnt FROM monthly_kid where day?=1 and age=?";

            stmt = con.prepareStatement(sql);
            stmt.setInt(1, day);
            stmt.setInt(2, age);


            ResultSet rs = stmt.executeQuery();

            while(rs.next()) {
            	cnt=rs.getInt("cnt");
            	}
            stmt.close();
        } catch (SQLException e) {
        	e.printStackTrace();
    		System.out.println("データの登録に失敗しました。");
    	} finally {
    		try {
    			if (stmt != null) {
    				stmt.close();
    			}
    			if (con != null) {
    				con.close();
    			}
    		} catch (SQLException e) {
    			e.printStackTrace();
    		}

        }
        return cnt;
    }
}
