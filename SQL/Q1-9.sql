

CREATE TABLE staff (
id int PRIMARY KEY AUTO_INCREMENT,
name varchar(20) NOT NULL,
day1 int,
day2 int,
day3 int,
day4 int,
day5 int,
day6 int,
day7 int,
certificate int,
memo text
);

CREATE TABLE daily_kid (
id int PRIMARY KEY AUTO_INCREMENT,
name varchar(20) NOT NULL,
day int NOT NULL,
age int NOT NULL,
memo text
);
